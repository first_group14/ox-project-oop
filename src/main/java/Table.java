/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author USER01
 */
public class Table {

    private char table[][] = {{'-', '-', '-'},
    {'-', '-', '-'},
    {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private int row, col, round = 0;
    private Player winner;
    private boolean finish = false;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.row = row;
            this.col = col;
            return true;
        }
        return false;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    public void checkWin() {
        round++;
        checkRow();
        checkCol();
        checkCross1();
        checkCross2();
        checkDraw();
    }

    public void checkRow() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[row][j] != currentPlayer.getName()) {
                    return;
                }
            }
        }
        finish = true;
        winner = currentPlayer;
        score();
    }

    public void score() {
        if (currentPlayer == playerX) {
            playerX.win();
            playerO.lose();
        } else {
            playerX.lose();
            playerO.win();
        }
    }

    public void checkCol() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[j][col] != currentPlayer.getName()) {
                    return;
                }
            }
        }
        finish = true;
        winner = currentPlayer;
        score();
    }

    public void checkCross1() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[j][j] != currentPlayer.getName()) {
                    return;
                }
            }
        }
        finish = true;
        winner = currentPlayer;
        score();
    }

    public void checkCross2() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0, m = 2; j < 3; j++, m--) {
                if (table[j][m] != currentPlayer.getName()) {
                    return;
                }
            }
        }
        finish = true;
        winner = currentPlayer;
        score();
    }

    public void checkDraw() {
        if (winner == null && round == 9) {
            playerX.draw();
            playerO.draw();
            finish = true;
        }

    }

    public void showTable() {
        System.out.println(" 1 2 3");
        for (int i = 0; i < 3; i++) {
            System.out.print((i + 1));
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    public Player getWinner() {
        return winner;
    }

    public boolean isFinish() {
        return finish;
    }

}
