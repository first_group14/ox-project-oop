
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author USER01
 */
public class Game {

    private Player playerX;
    private Player playerO;
    private Player turn;
    private Table table;
    private int row, col;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        System.out.println("Please input Row Col: ");
        row = kb.nextInt() - 1;
        col = kb.nextInt() - 1;
        checkInput();

    }

    public void checkInput() {
        if (row < 0 || row > 2 || col < 0 || col > 2) {
            input();
        } else {
            checkTableEmpty();
        }
    }

    public void checkTableEmpty() {
        if (table.setRowCol(row, col) == false) {
            input();
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }
    
    public void showBye(){
        System.out.println("Bye bye ....");
    }

    public void run() {
        this.showWelcome();
        this.showTable();
        while (true) {
            this.showTurn();
            this.input();
            this.showTable();    
            table.checkWin();
            if (table.isFinish()) {
                if (table.getWinner() == null) {
                    System.out.println("Draw....!!!");
                } else {
                    System.out.println("Player " + table.getWinner().getName() + " win....");
                }
                break;
            }
            table.switchPlayer();
        }
        this.showBye();
    }
}
